import React, { useState, useEffect } from 'react';
import Recipe from './Recipe'
import axios from 'axios';

const Search= () =>{

const APP_ID = "8d603356"
const APP_KEY = "8f6acc323ca44856aa348746d2fdd7d3"  


const [searchResultRecipes, setSearchResultRecipes ] = useState([])
const [selectedRecipe, setSelectedRecipe ] = useState({})
const [search, setSearch ] = useState("")
const [query , setQuery] = useState("chicken")
const [displayRecipe , setDisplayRecipe] = useState(false)


const getRecipes =()=>{
    console.log("called API")
    axios.get(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`)
    .then(res => {
        setSearchResultRecipes(res.data.hits)
    })

}
// getRecipes will be called at mounting stage and each time query state is changed 
useEffect(()=>{

    getRecipes() 
 }, [query])

const handelOnChange = (e) => {
  
    setSearch(e.target.value) 
}
const handelSubmit =(e)=>{
  
    e.preventDefault()

    setQuery(search)
    setSearch("") 
    setDisplayRecipe(false)
    
}
const handleSelectingRecipe =(e) =>{
 
    const nameOfSelectedRecipe = e.target.innerHTML
    const selectedRecipe = searchResultRecipes.find( recipe => recipe.recipe.label === nameOfSelectedRecipe )
    setSelectedRecipe(selectedRecipe)
    setDisplayRecipe(true)
   
}

    return (
      <div className="search">
       <form  onSubmit={handelSubmit} className="search-form">
          <input  
          className="search-input" 
          type="text"
          value = {search}
          onChange ={handelOnChange}
          />
          <button className="search-button" type="submit">search</button>
      </form>
      <ul>
            {searchResultRecipes.map((recipe , index)=>(
                <li key={index} onClick={handleSelectingRecipe} >{recipe.recipe.label}</li>
            ))}
       </ul>
       {displayRecipe  && 
            <Recipe recipe={selectedRecipe} />       
        }
             
      </div>
    );
}

export default Search;