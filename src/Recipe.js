import React from 'react';
import './App.css';
const  Recipe = ({ recipe })  => {
    
    const lable = recipe.recipe.lable
    const calories = recipe.recipe.calories
    const image = recipe.recipe.image
    const ingredients = recipe.recipe.ingredients
    return (
    <div className="recipe">
      <h1>{lable}</h1>
    <h3> calories : {Math.round(calories)} cal</h3>
    <img src={image} alt={lable} />
    <ol>
    {ingredients.map((ingredient, index) =>(
            <li key={index} >{ingredient.text}</li>
    ))}

    </ol>
  
    </div>
  );
}

export default Recipe;