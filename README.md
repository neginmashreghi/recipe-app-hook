## Description
Recipe application search and display recipes using [React Hook](https://reactjs.org/docs/hooks-intro.html) and [edamam recipe search API](https://developer.edamam.com/edamam-recipe-api)

---
## Hook

Hooks are a special function that let you use state and other React features to function components.

### State Hook 

[useState](https://reactjs.org/docs/hooks-state.html) is a Hook that lets you add React state to function components. 

useState returns a pair:  

*  current state value  
*  function that lets you update it

> You can call this function from an event handler or somewhere else  
> It’s similar to this.setState in a class, except it doesn’t merge the old and new state together. 
   
The only argument to useState is the initial state.The initial state argument is only used during the first render. 

You can use the State Hook more than once 

### Effect Hook 
 

[useEffect](https://reactjs.org/docs/hooks-effect.html) tell React that your component needs to do something after render. React will remember the function you passed, and call it later after performing the DOM updates 

 It serves the same purpose of

* componentDidMount, ComponentDidUpdate 

> By default useEffect runs both after the first render and after every update 

* componentWillUnmount  

> If your effect returns a function, React will run it when it is time to clean up 

in React classes, but unified into a single API 


cleaning up or applying the effect after every render might create a performance problem.You can tell React to skip applying an effect if certain values haven’t changed between re-renders. 

To do so, pass an array as an optional second argument to useEffect. 

> useEffect(() => { }, []); 

---

### Available Scripts

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### `npm test`

Launches the test runner in the interactive watch mode.

#### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

#### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you canít go back!**


